module bitbucket.org/vbus/nats-scenario-player

go 1.13

require (
	github.com/godbus/dbus v4.1.0+incompatible
	github.com/mitchellh/mapstructure v1.1.2
	github.com/nats-io/nats-server/v2 v2.1.2
	github.com/nats-io/nats.go v1.9.1
	github.com/pkg/errors v0.9.1
	github.com/sirupsen/logrus v1.4.2
	github.com/stretchr/testify v1.2.2
	golang.org/x/crypto v0.0.0-20200128174031-69ecbb4d6d5d // indirect
)
