// +build shared
// This file is use to produce a shared library.

package main

import (
	"C"
	"bitbucket.org/vbus/nats-scenario-player/player"
)

var p *player.NatsPlayer = nil

//export PlayScenario
func PlayScenario(id *C.char, file *C.char) *C.char {
	if p != nil {
		return C.CString("the player is already started, stop it before")
	}
	p = player.New(C.GoString(id))
	err := p.Load(C.GoString(file))
	if err != nil {
		return C.CString(err.Error())
	}

	err = p.Play()
	if err != nil {
		return C.CString(err.Error())
	}

	return C.CString("")
}

//export WaitDone
func WaitDone() *C.char {
	if p == nil {
		return C.CString("the player is not started")
	}

	p.WaitDone()

	return C.CString("")
}

//export IsSuccess
func IsSuccess() bool {
	if p == nil {
		return false
	}

	return p.IsSuccess()
}

//export Stop
func Stop() *C.char {
	if p == nil {
		return C.CString("the player is not started")
	}
	err := p.Stop()
	if err != nil {
		return C.CString(err.Error())
	}

	p = nil

	return C.CString("")
}

func main() {}