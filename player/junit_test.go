package player

import (
	"encoding/xml"
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestFoo(t *testing.T) {
	tests := testsuites{
		Duration: 0,
		Tests: []testsuite{
			{
				Failure: 0,
				Name:    "",
				Package: "",
				Tests:   0,
				Time:    0,
				Cases: []testcase{
					{
						Classname: "",
						Name:      "",
						Time:      0,
					},
				},
			},
		},
	}

	output, err := xml.MarshalIndent(tests, "", "    ")
	if err != nil {
		fmt.Printf("error: %v\n", err)
	}
	exp := `<testsuites duration="0">
    <testsuite failure="0" name="" package="" tests="0" time="0">
        <testcase classname="" name="" time="0"></testcase>
    </testsuite>
</testsuites>`
	assert.Equal(t, exp, string(output))
}
