package player

import (
	"github.com/nats-io/nats-server/v2/server"
	"github.com/pkg/errors"
)

type natsServer struct {
	server *server.Server
}

func newNatsServer(logger server.Logger) (*natsServer, error) {
	opts := &server.Options{
		Port: 21400,
	}

	// Create the server with appropriate options.
	serv, err := server.NewServer(opts)
	if err != nil {
		return nil, errors.Wrap(err, "unable to create nats server")
	}
	// Configure the logger based on the flags
	serv.ConfigureLogger()
	serv.SetLogger(logger, false, true)

	return &natsServer{
		server:serv,
	}, nil
}

func (s *natsServer) Start() {
	// Start things up. Block here until done.
	if err := server.Run(s.server); err != nil {
		server.PrintAndDie(err.Error())
	}
}

func (s *natsServer) Stop() {
	s.server.Shutdown()
}