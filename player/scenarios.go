package player

import (
	"encoding/json"
	"fmt"
	"github.com/mitchellh/mapstructure"
	"github.com/nats-io/nats.go"
	"github.com/pkg/errors"
	"io/ioutil"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"time"
)

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Scenario Steps
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Represents a scenario step.
type iStep interface {
	// Expand static variables used in this step.
	// Replace matcher with regex.
	Initialize(staticVariables strDict, storage strDict) error

	// Wait for this step to be successful by looking at nats log.
	Execute(context *stepExecContext) (error, strDict)

	// Log this step to console
	Log()

	SetTimeout(duration time.Duration)

	GetTimeout() time.Duration

	GetLongDesc() string

	SetIt(it string)

	GetDesc() string

	checkEqual(with *expectStep) (success bool, vars strDict)
}

// Hold required step execution context.
type stepExecContext struct {
	client   *nats.Conn
	logsChan natsLogChan
}

// Base struct for Step.
type stepBase struct { // iStep
	Timeout time.Duration
	It      string `mapstructure:"it"` // Additional description
}

// Convert a string to a regex by expanding variables.
// It replaces {name} for each key value in staticVariables dict.
// It replaces {str} and {int} with a regex to capture a string and a int.
// It replaces {str:id} and {int:id} with capture group to save these value for later.
func (p *stepBase) toRegex(value string, staticVariables strDict) *regexp.Regexp {
	// expand static variables
	regex := expandVariables(value, staticVariables)
	regex = regexp.QuoteMeta(regex)

	// replace string storage by regex: {str:name}
	strStoreRegex := regexp.MustCompile(`\\\{str:([^ ]+)\\\}`)
	regex = strStoreRegex.ReplaceAllString(regex, `(?P<$1>[^ ]+)`)

	// replace int storage by regex: {int:name}
	intStoreRegex := regexp.MustCompile(`\\\{int:([^ ]+)\\\}`)
	regex = intStoreRegex.ReplaceAllString(regex, `(?P<$1>\d+)`)

	// replace string matcher by regex: {str}
	regex = strings.Replace(regex, `\{str\}`, `[^ ]+`, -1)
	// replace int matcher by regex: {int}
	regex = strings.Replace(regex, `\{int\}`, `\d+`, -1)

	return regexp.MustCompile(regex) // finally store computer regex
}

// Wait to match a log line from nats server until it timeout.
// Returns true and stored variables
func waitUntilMatchOrTimeout(logsChan natsLogChan, onStep iStep) (error, strDict) {
	for true {
		select {
		case step := <-logsChan:
			// log.Tracef("received log line: %v", line)
			if expect, ok := step.(*expectStep); ok {
				ok, vars := onStep.checkEqual(expect)
				if ok {
					return nil, vars
				}
			}
		case <-time.After(onStep.GetTimeout()):
			return errors.New("timeout"), strDict{}
		}
	}
	return errors.New("wait internal error"), strDict{}
}

func (p *stepBase) SetTimeout(t time.Duration) {
	p.Timeout = t
}

func (p *stepBase) GetTimeout() time.Duration {
	return p.Timeout
}

func (p *stepBase) SetIt(it string) {
	p.It = it
}

func (p *stepBase) GetDesc() string {
	return p.It
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Expect Step
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// We expect something.
type expectStep struct { // implements iStep
	*stepBase
	Type  string         // one of "MSG_PAYLOAD", "PUB", "SUB", "MSG"
	What  string         `mapstructure:"what"` // A string describing the expectation
	Regex *regexp.Regexp // computed regex
}

// Creates a new expectStep.
func newExpectAction(timeout time.Duration) *expectStep {
	return &expectStep{
		stepBase: &stepBase{
			Timeout: timeout,
		},
	}
}

// Initialize regex
func (e *expectStep) Initialize(staticVariables strDict, storage strDict) error {
	e.Regex = e.toRegex(e.What, staticVariables) // init regex
	e.What = expandVariables(e.What, staticVariables)

	// Get the type
	if m := expectTypeRegex.FindStringSubmatch(e.What); len(m) > 1 {
		e.Type = m[1]
	} else {
		return errors.New("incorrect expect step: " + e.What)
	}

	return nil
}

func (e *expectStep) Execute(context *stepExecContext) (error, strDict) {
	return waitUntilMatchOrTimeout(context.logsChan, e)
}

func (e *expectStep) checkEqual(with *expectStep) (success bool, vars strDict) {
	if e.Type == natsMsgPayload {
		// when its a nats payload, if its json, same json object may be serialized in a different order.
		// So we need to parse the json to compare payload
		if m := msgPayloadRegex.FindStringSubmatch(e.What); len(m) > 1 {
			firstPayload := m[1]
			if m := msgPayloadRegex.FindStringSubmatch(with.What); len(m) > 1 {
				secondPayload := m[1]
				equal, err := areEqualJSON(firstPayload, secondPayload)
				if err != nil {
					return matchCapture(with.What, e.Regex) // default method
				}
				return equal, nil
			}
		}
		return matchCapture(with.What, e.Regex) // default
	} else {
		return matchCapture(with.What, e.Regex)
	}
}

func (e *expectStep) Log() {
	log.Debugf("Expect: %s", e.What)
}

func (e *expectStep) GetLongDesc() string {
	if len(e.What) > 100 {
		return "expect - " + e.What[0:100] + "..."
	} else {
		return "expect - " + e.What
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Publish Step
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// We want to publish something.
type publishStep struct { // implements iStep
	*stepBase
	Payload string `mapstructure:"payload"`
	Path    string `mapstructure:"path"`
}

// Creates a new publishStep.
func newPublishAction(timeout time.Duration) *publishStep {
	return &publishStep{
		stepBase: &stepBase{
			Timeout: timeout,
		},
	}
}

func (p *publishStep) Initialize(staticVariables strDict, storage strDict) error {
	p.Path = expandVariables(p.Path, staticVariables)
	p.Path = expandVariables(p.Path, storage)
	return nil
}

func (p *publishStep) Execute(context *stepExecContext) (error, strDict) {
	err := context.client.Publish(p.Path, []byte(p.Payload))
	context.client.Flush()
	return err, nil
}

func (p *publishStep) checkEqual(with *expectStep) (success bool, vars strDict) {
	return false, nil // to override
}


func (p *publishStep) Log() {
	log.Debugf("Publishing on %s with data %s", p.Path, p.Payload)
}

func (p *publishStep) GetLongDesc() string {
	return fmt.Sprintf("publish - %v", p.Path)
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Wait Step
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// We want to wait.
type waitStep struct { // implements iStep
	*stepBase
	Time int `mapstructure:"time"`
}

// Creates a new waitStep.
func newWaitAction(timeout time.Duration) *waitStep {
	return &waitStep{
		stepBase: &stepBase{
			Timeout: timeout,
		},
	}
}

func (p *waitStep) Initialize(staticVariables strDict, storage strDict) error {
	// do nothing
	return nil
}

func (p *waitStep) Execute(context *stepExecContext) (error, strDict) {
	time.Sleep(time.Millisecond * time.Duration(p.Time))
	return nil, nil
}

func (p *waitStep) checkEqual(with *expectStep) (success bool, vars strDict) {
	return false, nil // to override
}

func (p *waitStep) Log() {
	log.Debugf("Waiting for %v", time.Duration(p.Time))
}

func (p *waitStep) GetLongDesc() string {
	return fmt.Sprintf("wait - %v", p.Time)
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Scenario
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// A test scenario describe how to run a test.
type testScenario struct {
	Name        string
	Description string
	Steps       []iStep
}

// Creates a new scenario from a Json file.
func newTestScenarioFromFile(file string) (*testScenario, error) {
	jsonFile, err := os.Open(file)
	if err != nil {
		return nil, errors.Wrap(err, "cannot open scenario file")
	}

	bytes, err := ioutil.ReadAll(jsonFile)
	if err != nil {
		return nil, errors.Wrap(err, "cannot read scenario file")
	}

	// The scenario name is the file name without extension
	filename := filepath.Base(file)
	extension := filepath.Ext(filename)
	name := filename[0 : len(filename)-len(extension)]

	scenario, err := jsonToScenario(bytes, name)
	if err != nil {
		return nil, errors.Wrap(err, "cannot parse scenario file")
	}

	return scenario, nil
}

// Used for un-marshalling process because we don't know the type of iStep in advance.
type intermediateTestScenario struct {
	Name        string
	Description string                   `json:"description"`
	Steps       []map[string]interface{} `json:"steps"`
}

func jsonToScenario(bytes []byte, name string) (*testScenario, error) {
	var tmpScenario intermediateTestScenario
	err := json.Unmarshal(bytes, &tmpScenario)
	if err != nil {
		return nil, errors.Wrap(err, "cannot parse scenario file")
	}

	scenario := &testScenario{
		Name:        name,
		Description: tmpScenario.Description,
	}

	factories := map[string]func() iStep{
		"expect":  func() iStep { return newExpectAction(defaultStepTimeout) },
		"publish": func() iStep { return newPublishAction(defaultStepTimeout) },
		"wait":    func() iStep { return newWaitAction(defaultStepTimeout) },
	}

	for _, expect := range tmpScenario.Steps {
		// switch on "action" json key to determine which Step it is
		if stepType, ok := expect["action"]; ok {
			if factory, ok := factories[stepType.(string)]; ok {
				action := factory()
				err := mapstructure.Decode(expect, &action)
				if err != nil {
					return nil, errors.Wrap(err, "cannot decode expect action")
				}

				// if a timeout value is specified:
				if timeoutStr, ok := expect["timeout"]; ok {
					action.SetTimeout(time.Millisecond * time.Duration(timeoutStr.(float64)))
				}
				// it
				if it, ok := expect["it"]; ok {
					action.SetIt(it.(string))
				}

				scenario.Steps = append(scenario.Steps, action)
			} else {
				return nil, errors.New("unknown action name: " + stepType.(string))
			}
		} else {
			return nil, errors.New("invalid Json, acName key is missing")
		}
	}
	return scenario, nil
}
