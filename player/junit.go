package player

import (
	"encoding/xml"
)

type testsuites struct {
	Duration float64     `xml:"duration,attr"`
	Tests    []testsuite `xml:"testsuite"`
}

type testsuite struct {
	Failure int        `xml:"failure,attr"`
	Name    string     `xml:"name,attr"`
	Package string     `xml:"package,attr"`
	Tests   int        `xml:"tests,attr"`
	Time    float64    `xml:"time,attr"`
	Cases   []testcase `xml:"testcase"`
}

type testcase struct {
	Classname string  `xml:"classname,attr"`
	Name      string  `xml:"name,attr"`
	Time      float64 `xml:"time,attr"`
}

type jUnitFormatter struct {
	suites testsuites
}

func newJUnitFormatter(results []*TestResult) *jUnitFormatter {
	var suites testsuites

	// convert TestResult to Junit struct
	for _, res := range results {
		var suite testsuite
		var failureCount int = 0

		for _, stepRes := range res.StepResults {
			suite.Cases = append(suite.Cases, testcase{
				Classname: "",
				Name:      stepRes.Step.GetLongDesc(),
				Time:      float64(stepRes.Elapsed),
			})

			if stepRes.IsSuccess() {
				failureCount += 1
			}
		}

		suite.Tests = len(res.StepResults)
		suite.Time = float64(res.Elapsed)
		suite.Name = res.Scenario.Name
		suite.Failure = failureCount
		suites.Tests = append(suites.Tests, suite)
		suites.Duration = float64(res.Elapsed)
	}


	return &jUnitFormatter{suites: suites}
}

func (j jUnitFormatter) String() (string, error) {
	output, err := xml.MarshalIndent(j.suites, "", "	")
	return string(output), err
}
