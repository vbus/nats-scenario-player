package player

import (
	"context"
	"fmt"
	"github.com/pkg/errors"
	"io/ioutil"
	"strings"
	"sync"
)

// Library entry point.
type NatsPlayer struct {
	engine *testEngine
	//
	foreground bool
	started    bool
	cancel     context.CancelFunc
	wg         sync.WaitGroup
	//
	lastResults []*TestResult
}

// Creates a new nats player.
func New(remoteId string) *NatsPlayer {
	engine, err := newTestEngine(remoteId)
	if err != nil {
		panic(err)
	}

	return &NatsPlayer{
		engine:  engine,
		started: false,
	}
}

func (np *NatsPlayer) Load(scenarioFile string) error {
	return np.engine.loadScenariosFromFile(scenarioFile)
}

// Play in a goroutine.
func (np *NatsPlayer) Play() error {
	np.foreground = false
	if np.started {
		return errors.New("already started")
	}

	np.started = true

	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	np.cancel = cancel

	np.wg.Add(1)

	go func() {
		results, err := np.engine.ExecuteAll()
		if err != nil {
			log.Error(err.Error())
			np.wg.Done()
			return
		}

		np.lastResults = results
		np.wg.Done()
	}()

	return nil
}

// Play in a goroutine.
func (np *NatsPlayer) PlayForeground() error {
	np.foreground = true
	if np.started {
		return errors.New("already started")
	}

	np.started = true

	results, err := np.engine.ExecuteAll()
	if err != nil {
		return err
	}
	np.lastResults = results

	return nil
}

func (np *NatsPlayer) Stop() error {
	if !np.started {
		return errors.New("not started")
	}

	np.started = false

	if !np.foreground {
		np.cancel()
	}

	return nil
}

func (np *NatsPlayer) WaitDone() {
	if !np.foreground {
		np.wg.Wait()
	}
}

func (np *NatsPlayer) GetResults() []*TestResult {
	np.WaitDone()
	return np.lastResults
}

// Returns true if all test were successful.
func (np *NatsPlayer) IsSuccess() bool {
	for _, res := range np.lastResults {
		if !res.IsSuccess() {
			return false
		}
	}
	return true
}

// Expand variables in a string.
func expandVariables(varToExpand string, with map[string]string) (expanded string) {
	expanded = varToExpand
	for name, val := range with {
		expanded = strings.Replace(expanded, fmt.Sprintf("{%s}", name), val, -1)
	}
	return
}

func (np *NatsPlayer) WriteJUnitReport(file string) error {
	formatter := newJUnitFormatter(np.lastResults)
	data, err := formatter.String()
	if err != nil {
		return errors.Wrap(err, "cannot convert test result to JUnit")
	}

	err = ioutil.WriteFile(file, []byte(data), 0644)
	if err != nil {
		return errors.Wrap(err, "cannot write file")
	}

	return nil
}
