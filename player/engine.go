package player

import (
	"context"
	"fmt"
	"github.com/nats-io/nats.go"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"regexp"
	"strings"
	"time"
)

var (
	log = logrus.New()
	// This regex parses Nats server log line
	logLineRegex    = regexp.MustCompile(`[^ ]+ - cid:\d+ - (?:<<-)?(?:->>)? \[?(MSG_PAYLOAD|SUB|PUB|MSG):? +\[?(.+)\]$`)
	expectTypeRegex = regexp.MustCompile(`^(MSG_PAYLOAD|SUB|PUB|MSG)`)
	msgPayloadRegex = regexp.MustCompile(`^MSG_PAYLOAD: +\["(.*)"\]$`)
)

const (
	serviceId          = "test.runner"
	actionBufferSize   = 5000 // size of the buffered action channel
	defaultStepTimeout = 2000 * time.Millisecond
	natsMsgPayload     = "MSG_PAYLOAD"
	natsPublish        = "PUB"
	natsSubscribe      = "SUB"
	natsMessage        = "MSG"
)

type natsLogChan = chan iStep
type strDict = map[string]string

// Manage test scenarios.
type testEngine struct { // implements nats.Logger
	natsServer          *natsServer
	scenarios           []*testScenario
	client              *nats.Conn
	hostname            string
	remoteAppId         string      // remote app id: domain.name
	receivedNatsLogChan natsLogChan // contains received action, parsed from nats server logs
	//
	staticVariables strDict // available var to be expanded
}

func newTestEngine(remoteId string) (*testEngine, error) {
	log.SetLevel(logrus.TraceLevel)

	engine := &testEngine{
		remoteAppId:         remoteId,
		receivedNatsLogChan: make(natsLogChan, actionBufferSize),
	}

	serv, err := newNatsServer(engine)
	if err != nil {
		return nil, err
	}

	engine.natsServer = serv
	engine.hostname = getHostname()
	engine.staticVariables = strDict{
		"host":       engine.hostname,
		"remote_app": engine.remoteAppId,
	}

	return engine, nil
}

// Start test engine.
func (e *testEngine) start() error {
	go func() {
		e.natsServer.Start()
	}()
	time.Sleep(500 * time.Millisecond)

	client, err := connectNats()
	if err != nil {
		return errors.Wrap(err, "cannot connect to nats server")
	}
	e.client = client
	log.Info("connected to nats server")

	return nil
}

func (e *testEngine) stop() {
	e.natsServer.Stop()
}

// Wait until the remote app is ready.
// It waits for the ready path on nats.
func (e *testEngine) waitRemoteAppReady() (string, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second)
	defer cancel()

	// Synchronous subscriber with context
	sub, err := e.client.SubscribeSync(serviceId + ".ready")
	if err != nil {
		return "", errors.Wrap(err, "cannot subscribe sync")
	}

	msg, err := sub.NextMsgWithContext(ctx)
	if err != nil {
		return "", errors.Wrap(err, "app not ready")
	}

	return string(msg.Data), nil
}

func connectNats() (*nats.Conn, error) {
	return nats.Connect("nats://127.0.0.1:21400",
		nats.UserInfo("anonymous", "anonymous"),
		nats.Name("testrunner"))
}

// Run all scenarios.
func (e *testEngine) ExecuteAll() ([]*TestResult, error) {
	var results []*TestResult

	err := e.start()
	if err != nil {
		return nil, errors.Wrap(err, "cannot start engine")
	}

	for _, s := range e.scenarios {
		log.Infof("Running test '%s'", s.Name)
		res := e.ExecuteOne(s)
		res.Log()
		results = append(results, res)
	}

	e.stop()
	return results, nil
}

func logVariables(vars strDict) {
	for k, v := range vars {
		log.Debugf("%s = %s", k, v)
	}
}

// Creates the step execution context.
func (e *testEngine) getExecContext() *stepExecContext {
	return &stepExecContext{
		logsChan: e.receivedNatsLogChan,
		client:   e.client,
	}
}

// Execute one test.
func (e *testEngine) ExecuteOne(scenario *testScenario) *TestResult {
	start := time.Now()

	// contains stored variables
	storage := strDict{}
	var stepResults []*StepResult

	// Check steps in order
	for i, step := range scenario.Steps {
		log.Infof("Executing step %d:", i+1)
		stepStartTime := time.Now()

		err := step.Initialize(e.staticVariables, storage)
		if err != nil {
			stepResults = append(stepResults, NewFailedStepResult(
				step,
				errors.Wrap(err, "invalid json"),
				time.Since(stepStartTime)))
			break
		}
		step.Log()

		err, variables := step.Execute(e.getExecContext())
		if err == nil {
			if len(variables) > 0 {
				log.Debug("new variables:")
				logVariables(variables)
			}

			storage, err = mergeStrDict(storage, variables)
			if err != nil {
				stepResults = append(stepResults, NewFailedStepResult(
					step,
					errors.Wrap(err, "error while storing variable"),
					time.Since(stepStartTime)))
				break
			} else {
				stepResults = append(stepResults, NewSuccessStepResult(step, time.Since(stepStartTime)))
			}
		} else {
			stepResults = append(stepResults, NewFailedStepResult(
				step,
				errors.Wrap(err, "step failure"),
				time.Since(stepStartTime)))
			break
		}
	}

	return newTestResult(scenario, stepResults, time.Since(start))
}

// Load all scenario from a directory and add them to engine.
func (e *testEngine) loadScenariosFromDir(dir string) error {
	filesDirs, err := listFilesRecursively(dir)
	if err != nil {
		return errors.Wrap(err, "cannot list files in dir "+dir)
	}

	for _, fileDir := range filesDirs {
		ok, err := ifFile(fileDir)
		if err != nil {
			return errors.Wrap(err, "cannot determine if file is a dir or a file")
		}
		if !ok {
			continue // skip folder
		}
		err = e.loadScenariosFromFile(fileDir)
		if err != nil {
			return errors.Wrap(err, "cannot load scenario: "+fileDir)
		}
	}
	return nil
}

// Load a scenario from a file and add it into engine.
func (e *testEngine) loadScenariosFromFile(file string) error {
	scenario, err := newTestScenarioFromFile(file)
	if err != nil {
		return err
	}

	log.Infof("Scenario loaded: %s (from file: %s)", scenario.Name, file)
	e.scenarios = append(e.scenarios, scenario)
	return nil
}

// Expend variables contained in a string.
// Expandable variables are:
//   {host}       The hostname
//   {remote_app} The remote app id
func (e *testEngine) expandVariables(s string) (ret string) {
	ret = strings.Replace(s, "{host}", e.hostname, -1)
	ret = strings.Replace(s, "{remote_app}", e.remoteAppId, -1)
	return
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Intercept Nats Log
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

func (e *testEngine) Noticef(format string, v ...interface{}) {
}

func (e *testEngine) Warnf(format string, v ...interface{}) {
}

func (e *testEngine) Fatalf(format string, v ...interface{}) {
}

func (e *testEngine) Errorf(format string, v ...interface{}) {
	log.Errorf(format, v...)
}

func (e *testEngine) Debugf(format string, v ...interface{}) {
	log.Debugf(format, v...)
}

func (e *testEngine) Tracef(format string, v ...interface{}) {
	newFormat := strings.ReplaceAll(format, "%q", "\"%s\"")
	log.Tracef(newFormat, v...)

	what := parseLogLineToAction(fmt.Sprintf(newFormat, v...))
	if what != nil {
		// fix log line
		//if strings.Contains(what, "PUB ") {
		// sometime there are 2 spaces...
		//what = strings.ReplaceAll(what, "  ", " ")
		//}

		e.receivedNatsLogChan <- what
	}
}

// Retrieve the part between bracket in a Nats log line.
func parseLogLineToAction(entry string) iStep {
	if m := logLineRegex.FindStringSubmatch(entry); len(m) > 1 {
		action := m[1]

		switch action {
		case natsMessage:
			return &expectStep{
				stepBase: &stepBase{},
				What:     "MSG " + m[2],
				Type:     natsMessage,
			}
		case natsMsgPayload:
			return &expectStep{
				stepBase: &stepBase{},
				What:     "MSG_PAYLOAD: [" + m[2] + "]",
				Type:     natsMsgPayload,
			}
		case natsPublish:
			return &expectStep{
				stepBase: &stepBase{},
				What:     "PUB " + strings.Join(strings.Fields(m[2]), " "),
				Type:     natsPublish,
			}
		case natsSubscribe:
			return &expectStep{
				stepBase: &stepBase{},
				What:     "SUB " + m[2],
				Type:     natsSubscribe,
			}
		default:
			return nil
		}
	}
	return nil
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Test Result
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Contains step results.
type StepResult struct {
	Step    iStep
	Error   error // error in case of failure
	Elapsed time.Duration
}

func NewFailedStepResult(step iStep, error error, elapsed time.Duration) *StepResult {
	return &StepResult{Step: step, Error: error, Elapsed: elapsed}
}

func NewSuccessStepResult(step iStep, elapsed time.Duration) *StepResult {
	return &StepResult{Step: step, Elapsed: elapsed, Error: nil}
}

func (sr *StepResult) IsSuccess() bool {
	return sr.Error == nil
}

// Contains test results.
type TestResult struct {
	Scenario    *testScenario
	Elapsed     time.Duration
	StepResults []*StepResult
}

func newTestResult(scenario *testScenario, steps []*StepResult, elapsed time.Duration) *TestResult {
	return &TestResult{
		Scenario:    scenario,
		Elapsed:     elapsed,
		StepResults: steps,
	}
}

func (t *TestResult) IsSuccess() bool {
	for _, s := range t.StepResults {
		if !s.IsSuccess() {
			return false
		}
	}
	return true
}

// Log this result to logger.
func (t *TestResult) Log() {
	lines := strings.Split(t.String(), "\n")
	for _, l := range lines {
		log.Info(l)
	}
}

// Stringer
func (t *TestResult) String() string {
	var lines []string
	if t.IsSuccess() {
		lines = append(lines, fmt.Sprintf("--- OK - %s - Total time: %v", t.Scenario.Name, t.Elapsed))
	} else {
		lines = append(lines, fmt.Sprintf("--- KO - %s - Total time: %v", t.Scenario.Name, t.Elapsed))
	}
	lines = append(lines, "---------------------------------------")

	for i, stepRes := range t.StepResults {
		if stepRes.IsSuccess() {
			lines = append(lines, fmt.Sprintf("    --- OK - step %v - it %s (%v)", i, stepRes.Step.GetDesc(), stepRes.Elapsed))
			lines = append(lines, fmt.Sprintf("           - %s", stepRes.Step.GetLongDesc()))
		} else {
			lines = append(lines, fmt.Sprintf("    --- KO - step %v - it %s (%v)", i, stepRes.Step.GetDesc(), stepRes.Elapsed))
			lines = append(lines, fmt.Sprintf("           - %s", stepRes.Step.GetLongDesc()))
		}
	}
	return strings.Join(lines, "\n")
}
