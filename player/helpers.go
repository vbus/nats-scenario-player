package player

import (
	"encoding/json"
	"fmt"
	"github.com/godbus/dbus"
	"github.com/pkg/errors"
	"os"
	"path/filepath"
	"reflect"
	"regexp"
	"strings"
)


func listFilesRecursively(searchDir string) ([]string, error) {
	var fileList []string
	err := filepath.Walk(searchDir, func(path string, f os.FileInfo, err error) error {
		fileList = append(fileList, path)
		return nil
	})
	if err != nil {
		return nil, errors.Wrap(err, "cannot walk directory")
	}

	for _, file := range fileList {
		fmt.Println(file)
	}
	return fileList, nil
}

func ifFile(file string) (bool, error) {
	fi, err := os.Stat(file)
	if err != nil {
		return false, err
	}
	mode := fi.Mode()
	return mode.IsRegular(), nil
}

// Retrieve the hostname
func getHostname() string {
	hostnameLocal, _ := os.Hostname()
	hostname := strings.Split(hostnameLocal, ".")[0]

	dbusConn, err := dbus.SystemBus()
	if err != nil {
		log.Warn("cannot connect to dbus: ", err)
	} else {
		obj := dbusConn.Object("io.veea.VeeaHub.Info", "/io/veea/VeeaHub/Info")
		call := obj.Call("io.veea.VeeaHub.Info.Hostname", 0)
		err = call.Store(&hostname)
	}

	return hostname
}

func mergeStrDict(d1 strDict, d2 strDict) (res strDict, err error) {
	res = make(strDict)
	for k, v := range d1 {
		res[k] = v
	}
	for k, v := range d2 {
		if _, found := res[k]; found {
			return res, errors.New(fmt.Sprintf("duplicate keys: %s", k))
		}
		res[k] = v
	}
	return
}

// Match a regex against a string and capture groups.
func matchCapture(logLine string, regex *regexp.Regexp) (bool, strDict) {
	match := regex.FindStringSubmatch(logLine)

	if len(match) > 0 { // string match
		// get named match (stored values)
		result := make(strDict)
		for i, name := range regex.SubexpNames() {
			if i != 0 && name != "" {
				result[name] = match[i]
			}
		}
		log.Debugf("match: %s", logLine)
		return true, result
	}
	return false, strDict{}
}

func areEqualJSON(s1, s2 string) (bool, error) {
	var o1 interface{}
	var o2 interface{}

	var err error
	err = json.Unmarshal([]byte(s1), &o1)
	if err != nil {
		return false, fmt.Errorf("Error mashalling string 1 :: %s", err.Error())
	}
	err = json.Unmarshal([]byte(s2), &o2)
	if err != nil {
		return false, fmt.Errorf("Error mashalling string 2 :: %s", err.Error())
	}

	return reflect.DeepEqual(o1, o2), nil
}
