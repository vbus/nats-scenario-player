import * as ffi from "ffi-napi"
import * as path from "path";

const libnatsplayer = ffi.Library(path.join(__dirname, 'libnatsplayer'), {
    'PlayScenario': ['string', ['string', 'string']],
    'WaitDone': ['string', []],
    'IsSuccess': ['int', []],
    'Stop': ['string', []],
});

// Nats player binding
export class Player {
    private readonly id: string;

    constructor(id: string) {
        this.id = id;
    }

    playScenario(file: string) {
        const err = libnatsplayer.PlayScenario(this.id, file);
        if (err !== "") {
            throw Error(err)
        }
    }

    async waitDone() {
        const err = libnatsplayer.WaitDone();
        if (err !== "") {
            throw Error(err)
        }
    }

    isSuccess(): boolean {
        const success = libnatsplayer.IsSuccess();
        return success === 1
    }

    stop() {
        const err = libnatsplayer.Stop();
        if (err !== "") {
            throw Error(err)
        }
    }
}