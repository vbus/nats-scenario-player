import subprocess
from distutils.core import setup
from setuptools.command.install import install
import distutils.cmd
import distutils.log


# this is here because when its moved inside the build command, the files are not packaged.
subprocess.check_call(['go', 'build', '-tags', 'shared', '-o', 'libnatsplayer.so', '-buildmode=c-shared', 'shared.go'])
subprocess.check_call(['cp', 'libnatsplayer.so', './python/natsplayer'])


class CustomInstallCommand(install):
    def run(self):
        install.run(self)


setup(
    name='natsplayer',
    description='this is a nats-player binding of the go library',
    version='',
    packages=['natsplayer'],
    package_dir={'': 'python'},
    url='',
    license='',
    author='boolangery',
    author_email='',
    cmdclass={
        'install': CustomInstallCommand,
    },
    package_data={'': ['libnatsplayer.so']},
    zip_safe=False,
)
