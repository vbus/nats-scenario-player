"""
    Python binding of nats-scenario-player.
"""
import os
from ctypes import cdll, c_char_p, c_bool

base_dir = os.path.abspath(os.path.dirname(__file__))
lib_path = os.path.join(base_dir, "libnatsplayer.so")


class Player:
    def __init__(self, app_id: str):
        self.app_id = app_id
        # setup shared library binding
        player_lib = cdll.LoadLibrary(lib_path)
        # PlayScenario
        play_scenario = player_lib.PlayScenario
        play_scenario.argtypes = [c_char_p, c_char_p]
        play_scenario.restype = c_char_p
        self._play_scenario = play_scenario
        # WaitDone
        wait_done = player_lib.WaitDone
        wait_done.argtypes = []
        wait_done.restype = c_char_p
        self._wait_done = wait_done
        # IsSuccess
        is_success = player_lib.IsSuccess
        is_success.argtypes = []
        is_success.restype = c_bool
        self._is_success = is_success
        # Stop
        stop = player_lib.Stop
        stop.argtypes = []
        stop.restype = c_char_p
        self._stop = stop

    def play(self, scenario_file: str) -> 'Player':
        err = self._play_scenario(str.encode(self.app_id), str.encode(scenario_file))
        if err != b'':
            raise Exception(err.decode())
        return self

    def wait_done(self) -> 'Player':
        err = self._wait_done()
        if err != b'':
            raise Exception(err.decode())
        return self

    def is_success(self) -> bool:
        return self._is_success()

    def stop(self) -> 'Player':
        err = self._stop()
        if err != b'':
            raise Exception(err.decode())
        return self
