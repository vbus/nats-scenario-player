# Nats Scenario Player

This is a tool to validate exchange between a client application and a Nats server.

The nats-scenario-player act as a Nats server where your must connect to.

This tool can be used directly inside Go source.

## Usage

    $ nats-scenario-player [appId] [scenarioFile]
    
Example:

    $ nats-scenario-player test.vbusgo ./my-scenario.json
    
## What is a scenario ?

A scenario is a Json file describing what action we expect to be produced by one test.
An action is for example a Nats publish.

The minimal scenario structure is:
```json
{
  "description": "test the ask permission command",
  "steps": [
    [...]
  ]
}
```
Then you will have to add some step to your scenario. Each steps must pass for the test to be considered successful.

## Scenario steps

These steps are available to describe your scenario.

### Expect Step

This step tells to the scenario-player to wait for an event on Nats. The Json structure is:

```json
{
  "action": "expect",
  "what": "PUB system.foo.bar {str:inbox_1} {int}"
}
```

The `what` field describe something to be expected on Nats. The format is the same as the one we can find in nats-server.log.
You can also use variables inside the `what` field (see Variables section).

Examples:

Expect a publish and store inbox value to inbox_1:
```json
{
  "action": "expect",
  "what": "PUB system.authorization.{host}.{remote_app}.{host}.permissions.set {str:inbox_1} {int}"
}
```

Expect a payload:
```json
{
  "action": "expect",
  "what": "MSG_PAYLOAD: [\"{\\\"subscribe\\\":[\\\"{remote_app}\\\",\\\"{remote_app}.>\\\",\\\"should.be.true\\\"],\\\"publish\\\":[\\\"{remote_app}\\\",\\\"{remote_app}.>\\\",\\\"should.be.true\\\"]}\\n\"]"
}
```

### Publish Step

This step performs a Nats publish.

The shape is:

```json
{
  "action": "publish",
  "payload": "true",
  "path": "system.foo"
}
```

You can use variables inside the `path` field.

Examples:

Wait for a request on `system.foo`, check the payload content, then respond on the saved inbox with a publish:
```json
{
  "description": "test the ask permission command",
  "steps": [
    {
      "action": "expect",
      "what": "PUB system.double {str:inbox_1} {int}"
    },
    {
      "action": "expect",
      "what": "MSG_PAYLOAD: [\"42\"]"
    },
    {
      "action": "publish",
      "payload": "84",
      "path": "{inbox_1}"
    }
  ]
}
```

### Wait Step

This step wait for the specified time in milliseconds.

```json
{
  "action": "wait",
  "time": 500
}
```

## Variables

When describing actions, you can use some predefined variables.
These variables will be expanded as below:
* {host}:       the actual host
* {remote_app}: the app name provided as first arg in the cli tool

Examples:

I write:

    system.authorization.{host}.{remote_app}
    
Lets say my hostname is foo-PC:

    * nats-scenario-player test.me ./my-scenario.json
    
Its expanded to:

    system.authorization.foo-PC.test.me

## Storage

In addition to that, you can store values to be used later:
* {str:id}: the string will be stored with this id
* {int:id}: the integer will be stored with this id

Examples:

I want to store the inbox to be used in another step, I write:

```json
{
  "action": "expect",
  "what": "PUB system.double {str:inbox_1} {int}"
}
```



