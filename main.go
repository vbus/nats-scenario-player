package main

import (
	"bitbucket.org/vbus/nats-scenario-player/player"
	"flag"
	"fmt"
	"log"
	"os"
)

func usage() {
	fmt.Println("nats-scenario-player [appId] [scenarioFile]")
}

func checkErr(err error) {
	if err != nil {
		log.Fatal(err.Error())
	}
}

func main() {
	args := os.Args[1:]

	if len(args) != 2 {
		usage()
		panic("invalid arguments")
	}
	appId := args[0]
	file := args[1]

	var junit bool
	flag.BoolVar(&junit, "junit", false, "generate a Junit xml test report")

	p := player.New(appId)
	checkErr(p.Load(file))
	checkErr(p.PlayForeground())

	if junit {
		checkErr(p.WriteJUnitReport("junit_report.xml"))
	}

	if p.IsSuccess() {
		os.Exit(0)
	} else {
		os.Exit(1)
	}
}
